import 'package:flutter/material.dart';


class FeaturePageOne extends StatelessWidget {
  const FeaturePageOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Feature Page One')),
    );
  }
}
