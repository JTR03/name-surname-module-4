import 'package:flutter/material.dart';
import 'package:my_app/constants/routes.dart';
import 'package:my_app/screens/dashboard.dart';
import 'package:my_app/screens/feature_one.dart';
import 'package:my_app/screens/feature_two.dart';
import 'package:my_app/screens/login.dart';
import 'package:my_app/screens/registration.dart';
import 'package:my_app/screens/user_profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        scaffoldBackgroundColor: const Color.fromARGB(255, 242, 245, 247),
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.blueGrey,
          ).copyWith(
            secondary: const Color.fromARGB(255, 248, 208, 110),
          ),
          textTheme:
              const TextTheme(bodyText2: TextStyle(color: Colors.purple)),
      ),
      home: const LoginPage(),
      routes: {
        loginRoute:(context) => const LoginPage(),
        registerRoute: (context) => const RegisterPage(),
        dashboardRoute:(context) => const DashboardPage(),
        featureOneRoute: (context) => const FeaturePageOne(),
        featureTwoRoute: (context)=> const FeaturePageTwo(),
        userProfileRoute: (context) => const EditProfile(),
      }
    );
  }
}


